



Pact Caster

Fiend Expanded Spells


Dark One’s Blessing



Eldritch Invocations
- Agonizing Blast
- Armor of Shadows
- Ascendant Step(9)
- Beast Speech
- Beguiling Influence
- Bewitching Whispers(7)
- Book of Ancient Secrets(Tome)
- Chains of Carceri(15)(Chain)
- Devil’s Sight
- Dreadful Word(7)
- Eldritch Sight
- Eldritch Spear
- Eyes of the Rune Keeper
- Fiendish Vigor
- Gaze of Two Minds
- Lifedrinker(12)(Blade)
- Mask of Many Faces
- Master of Myriad Forms(15)
- Minions of Chaos(9)
- Mire the Mind(5)
- Misty Visions
- One with Shadows(5)
- Otherworldly Leap(9)
- Repelling Blast
- Sculptor of Flesh(7)
- Sign of Ill Omen(5)
- Thief of Five Fates
- Thirsting Blade(5)(Blade)
- Visions of Distant Realms(15)
- Voice of the Chain Master(chain)
- Whispers of the Grave(9)
- Witch Sight(15)
- 


Pact Boon
- Pact of the Chain
- Pact of the Blade
- Pact of the Tome


Dark One’s Own Luck


Fiendish Resilience



Mystic Arcanum


Hurl Through Hell


Eldritch Master