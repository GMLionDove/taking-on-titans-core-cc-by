Things to consider:
	- Level 5 and below PCs must defeat an Elite to level up.
	- Level 6 and higher PCs must defeat a Boss to level up.
	- PCs can only level up once per Downtime.
	- PCs can only level up in a safe hub(town).
	- Excess XP is pooled and used. (See [[XP Changes]])
	- XP is reset to 0 (Zero) after every level up.
#### Level 2 to 3:
- **XP Needed**: 600 XP
- **Encounters**: 
  - **CR1 Elite**: 200 XP
  - **Additional Encounters**: 400 XP (2x CR1 Monsters: 200 XP each)
- **Total XP Earned**: 600 XP
- **Excess XP**: 0 XP

#### Level 3 to 4:
- **XP Needed**: 1,800 XP
- **Encounters**:
  - **CR3 Elite**: 700 XP
  - **Additional Encounters**: 1,100 XP (3x CR1 Monsters: 200 XP each + 2x CR2 Monsters: 450 XP each)
- **Total XP Earned**: 1,800 XP
- **Excess XP**: 0 XP

#### Level 4 to 5:
- **XP Needed**: 3,800 XP
- **Encounters**:
  - **CR4 Elite**: 1,100 XP
  - **Additional Encounters**: 2,700 XP (4x CR2 Monsters: 450 XP each + 1x CR3 Monster: 700 XP)
- **Total XP Earned**: 3,800 XP
- **Excess XP**: 0 XP

#### Level 5 to 6:
- **XP Needed**: 7,500 XP
- **Encounters**:
  - **CR6 Boss**: 2,300 XP
  - **2x CR3 Elites**: 1,400 XP
  - **Additional Encounters**: 3,800 XP (4x CR3 Monsters: 700 XP each + 1x CR2 Monster: 450 XP)
- **Total XP Earned**: 7,500 XP
- **Excess XP**: 0 XP

#### Level 6 to 7:
- **XP Needed**: 9,000 XP
- **Encounters**:
  - **CR6 Uber**: 4,600 XP
  - **CR8 Boss**: 3,900 XP
  - **2x CR4 Elites**: 2,200 XP
  - **Additional Encounters**: 2,300 XP (3x CR2 Monsters: 450 XP each + 2x CR1 Monsters: 200 XP each)
- **Total XP Earned**: 13,000 XP
- **Excess XP**: 4,000 XP

#### Level 7 to 8:
- **XP Needed**: 11,000 XP
- **Encounters**:
  - **CR9 Boss Pair**: 5,000 XP
  - **2x CR4 Elites**: 2,200 XP
  - **Additional Encounters**: 4,000 XP (2x CR4 Monsters: 1,100 XP each + 4x CR2 Monsters: 450 XP each)
- **Total XP Earned**: 11,200 XP
- **Excess XP**: 200 XP

#### Level 8 to 9:
- **XP Needed**: 14,000 XP
- **Encounters**:
  - **CR10 Boss**: 5,900 XP
  - **2x CR4 Elites**: 2,200 XP
  - **Additional Encounters**: 6,000 XP (4x CR4 Monsters: 1,100 XP each + 2x CR3 Monsters: 700 XP each)
- **Total XP Earned**: 14,100 XP
- **Excess XP**: 100 XP

#### Level 9 to 10:
- **XP Needed**: 16,000 XP
- **Encounters**:
  - **CR12 Boss**: 8,400 XP
  - **2x CR4 Elites**: 2,200 XP
  - **Additional Encounters**: 6,000 XP (5x CR3 Monsters: 700 XP each + 2x CR4 Monsters: 1,100 XP each)
- **Total XP Earned**: 16,600 XP
- **Excess XP**: 600 XP

#### Level 10 to 11:
- **XP Needed**: 21,000 XP
- **Encounters**:
  - **CR13 Boss**: 10,000 XP
  - **2x CR5 Elites**: 3,600 XP
  - **Additional Encounters**: 7,500 XP (4x CR4 Monsters: 1,100 XP each + 5x CR3 Monsters: 700 XP each)
- **Total XP Earned**: 21,100 XP
- **Excess XP**: 100 XP

#### Level 11 to 12:
- **XP Needed**: 15,000 XP
- **Encounters**:
  - **CR15 Boss**: 13,000 XP
  - **2x CR6 Elites**: 4,600 XP
  - **Additional Encounters**: 2,000 XP (2x CR4 Monsters: 1,100 XP each)
- **Total XP Earned**: 19,600 XP
- **Excess XP**: 4,600 XP

#### Level 12 to 13:
- **XP Needed**: 20,000 XP
- **Encounters**:
  - **CR15 Uber**: 26,000 XP
  - **2x CR6 Elites**: 4,600 XP
  - **CR16 Boss**: 15,000 XP
  - **Additional Encounters**: 5,000 XP (3x CR3 Monsters: 700 XP each + 2x CR4 Monsters: 1,100 XP each)
- **Total XP Earned**: 50,600 XP
- **Excess XP**: 30,600 XP

#### Level 13 to 14:
- **XP Needed**: 20,000 XP
- **Encounters**:
  - **CR17 Boss**: 18,000 XP
  - **2x CR6 Elites**: 4,600 XP
  - **Additional Encounters**: 2,000 XP (2x CR3 Monsters: 700 XP each + 2x CR2 Monsters: 450 XP each)
- **Total XP Earned**: 24,600 XP
- **Excess XP**: 4,600 XP

#### Level 14 to 15:
- **XP Needed**: 25,000 XP
- **Encounters**:
  - **CR17 Uber**: 54,000 XP
  - **2x CR7 Elites**: 5,000 XP
  - **Additional Encounters**: 7,000 XP (4x CR4 Monsters: 1,100 XP each + 1x CR3 Monster: 700 XP)
- **Total XP Earned**: 66,000 XP
- **Excess XP**: 41,000 XP

#### Level 15 to 16:
- **XP Needed**: 30,000 XP
- **Encounters**:
  - **CR17 Omega**: 135,000 XP
  - **2x CR7 Elites**: 5,000 XP
- **Total XP Earned**: 140,000 XP
- **Excess XP**: 110,000 XP
