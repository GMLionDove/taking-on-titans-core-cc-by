### Rules for Uber Items and Omega Items

**Uber Items**:
- **Acquisition**: Uber items are acquired by defeating Uberbosses.
- **Modifiers**: Uber items can have modifiers ranging from +1 to +5, depending on the CR of the Uberboss defeated.
- **Upgrades**: Each Uber item can be upgraded as characters defeat higher CR Uberbosses.
- **Usage**: A character can only attune to a limited number of Uber items, typically one or two, to maintain balance.

**Omega Items**:
- **Acquisition**: Omega items are acquired by defeating Omegabosses.
- **Modifiers**: Omega items can have modifiers ranging from +1 to +10, depending on the CR of the Omegaboss defeated.
- **Upgrades**: Each Omega item can be upgraded as characters defeat higher CR Omegabosses.
- **Usage**: A character can only attune to one Omega item due to their immense power.

Uber Items:
CR 6-8    ────────── +1
CR 9-12   ────────── +2
CR 13-16  ────────── +3
CR 17-20  ────────── +4
CR 21-24  ────────── +5

Omega Items:
CR 17-20  ────────── +1 to +3
CR 21-24  ────────── +4 to +6
CR 25-28  ────────── +7 to +8
CR 29-32  ────────── +9
CR 33+    ────────── +10



### Uber Modifiers

1. **Threatening +1 to +5**:
   - **Effect**: Items add their Threatening modifier to the threat range of attack rolls, increasing crit chance. Additionally, the number of base weapon damage dice rolled is multiplied by the modifier (but not bonus damage such as sneak attack, smite, or added elemental damage).

2. **Superior Resistance +1 to +10**:
   - **Effect**: Grants resistance and removes half damage from successful saves. Additionally, resisted damage is further reduced by a number of d10's equal to the Superior Resistance Modifier. If an attack would ignore this resistance, its typed damage is still reduced by the Superior Resistance Modifier dice.

3. **Simulcast +1 to +10 (spell)**:
   - **Effect**: Whenever a spell is cast using this item as a focus, (spell) is also cast at (modifier × 3) level.

4. **Wasteless +1 to +5**:
   - **Effect**: If an ability or spell fails due to any event other than a roll of natural 1 or 20, it is unspent instead. Wasteless can be used a number of times equal to its modifier per day.

5. **Vast**:
   - **Effect**: Effects from this item are granted a burst area effect equal to the item's highest modifier × 10 ft. For effects that already have a range of an area, the range is tripled instead.

6. **Aggressive +1 to +3**:
   - **Effect**: If the wielder has the extra attack feature, gain an additional attack equal to the modifier. If not, grants extra attacks equal to the modifier, one of which may be a cantrip. For characters with extra attack, they can make an extra attack as a bonus action.



---------------------------------------------------------

### Minion-Bane Uber Modifiers

1. **Minion Slayer +1 to +5**:
   - **Effect**: When you hit a minion with an attack, the minion must succeed on a Constitution saving throw (DC 10 + modifier) or be instantly slain. For minions that are immune to being killed instantly, they instead take additional damage equal to the modifier multiplied by 10.

2. **Suppressing Aura +1 to +5**:
   - **Effect**: Minions within a radius of 30 feet per modifier are unable to use their legendary actions, reactions, or special abilities. Additionally, minions within this radius have disadvantage on all saving throws.

3. **Devastating Blow +1 to +5**:
   - **Effect**: Attacks made with this weapon against minions deal additional damage equal to twice the modifier. If the minion's HP is reduced to half or less by this attack, it is instantly destroyed.

4. **Nullifying Presence +1 to +5**:
   - **Effect**: Minions within a radius of 20 feet per modifier lose any resistances or immunities they possess. Additionally, minions within this radius must succeed on a Wisdom saving throw (DC 10 + modifier) at the start of their turn or be unable to take any actions or reactions until the start of their next turn.

5. **Crippling Strike +1 to +5**:
   - **Effect**: When you hit a minion with an attack, the minion's speed is reduced to 0, and it cannot benefit from bonuses to its speed until the end of its next turn. Additionally, the minion has disadvantage on attack rolls and ability checks until the end of its next turn.

6. **Warding Shield +1 to +5**:
   - **Effect**: While holding this shield, you and allies within 30 feet per modifier have resistance to all damage dealt by minions. Minions that deal damage to you or your allies within this radius take psychic damage equal to the modifier multiplied by 10.

7. **Banishing Strike +1 to +5**:
   - **Effect**: When you hit a minion with an attack, the minion must succeed on a Charisma saving throw (DC 10 + modifier) or be banished to a harmless demiplane for 1 minute. While banished in this way, the minion is incapacitated. The minion can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

8. **Disrupting Force +1 to +5**:
   - **Effect**: When you hit a minion with an attack, the minion's damage output is reduced by the modifier multiplied by 10 until the end of its next turn. Additionally, minions affected by this weapon cannot deal critical hits.

9. **Annihilating Wave +1 to +5**:
   - **Effect**: Once per long rest, you can release a wave of energy in a radius of 10 feet per modifier. Minions within this radius must succeed on a Constitution saving throw (DC 10 + modifier) or be instantly destroyed. Minions that succeed on the saving throw take damage equal to the modifier multiplied by 20.

10. **Deterring Presence +1 to +5**:
   - **Effect**: Minions within a radius of 30 feet per modifier have disadvantage on all attack rolls against you and your allies. Additionally, if a minion starts its turn within this radius, it must succeed on a Wisdom saving throw (DC 10 + modifier) or be frightened of you until the end of its next turn.





------------------







### Omega Modifiers

1. **Sovereign Immunity +1 to +10**:
   - **Effect**: Immunity to this damage type cannot be mitigated by attackers less powerful than CR 10 × Sovereign Immunity Modifier. Instead of taking damage of the immune type, the wearer is healed for that much damage and gains a typed damage shield in rolled d10's equal to the Sovereign Immunity Modifier. This shield acts as temporary HP that is reduced before HP from all sources and deals the immunity typed damage to melee attackers.

2. **Ferocity 2x, 3x**:
   - **Effect**: If the wielder has extra attack, multiplies total extra attacks. If not, grants three extra attacks, one of which can be a spell and one of which can be a cantrip. For characters with extra attack, they can make extra attacks as a bonus action.

3. **Divine Retribution +1 to +10**:
   - **Effect**: When reduced to 0 HP, you can explode in a burst of energy, dealing a number of d10s of damage equal to the modifier to all creatures within 30 feet. You are then restored to half your maximum HP.

4. **Temporal Mastery**:
   - **Effect**: Once per long rest, take an additional turn immediately after your current turn. The number of uses per day is equal to the modifier.

5. **Arcane Omniscience**:
   - **Effect**: You can cast any spell from any spell list up to a level equal to 2 + (modifier / 2) without needing material components once per long rest.

6. **Chrono Control**:
   - **Effect**: Once per short rest, you can manipulate time to either take an additional action or force an enemy to skip their turn. The number of uses per day is equal to the modifier.



