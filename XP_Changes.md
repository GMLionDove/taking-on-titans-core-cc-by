Things to consider:
	- Level 5 and below PCs must defeat an Elite to level up.
	- Level 6 and higher PCs must defeat a Boss to level up.
	- PCs can only level up once per Downtime.
	- PCs can only level up in a safe hub(town).
	- Excess XP is pooled and used.
	- XP is reset to 0 (Zero) after every level up.

# Excess XP Pool


**Distribute to Secondary Characters**
	    - No more than half the XP required for the leveling up character can be distributed to Secondary Characters.
	    - Secondary Characters cannot level up more times at once than their starting proficiency bonus, regardless of excess XP amount.
	    - After leveling up from XP Pool, Secondary Characters cannot gain more levels until after they go on a quest or bounty.
	    - Secondary Characters cannot progress beyond their leveled Tier of Play.
	    - To progress the to the next tier, ALL characters must defeat an Elite(-5) or Boss(+6) and spend Downtime in Town.
	    - **Unspendable XP is LOST after Downtime Ends**
	          

**Upgrade Town, Ship, Housing, Domain**
		- A minimum tax of 25% excess XP will be spent on the hub being leveled up.
		- Upgrading required to unlock:
			- Shops, better inventory, lower prices.
			- Guilds, PC factions, Followers.
			- Faster and Safer Travel Methods.
			- Town Rituals and Spell Services.
			- Custom Magic Item Forge.
			- Spell Creation Tower.
			- Alchemical Labratory.
			- Augment Mystic.
			- Private Housing.
			- Domain Play.
			- Short Term Boons.(Chef, Blessing, etc)


