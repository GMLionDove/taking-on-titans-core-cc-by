
##### (Formulas at Bottom)





### CR 1
**Normal**: AC/DC 12, HP 20, ATK +3, DPR 7, SV +3  
**Artillery**: AC/DC 10, HP 16, ATK +3, DPR 3, SV +1, AOE DPR 3  
**Brute**: AC/DC 10, HP 24, ATK +5, DPR 8.4, SV +3  
**Elite**: AC/DC 13, HP 27, ATK +3, DPR 9.45, SV +4  
**Minion**: AC/DC 10, HP 8, ATK +3, DPR 7, SV +1  

### CR 2
**Normal**: AC/DC 13, HP 40, ATK +4, DPR 14, SV +4  
**Artillery**: AC/DC 11, HP 32, ATK +4, DPR 8.4, SV +2, AOE DPR 6  
**Brute**: AC/DC 11, HP 48, ATK +6, DPR 16.8, SV +4  
**Elite**: AC/DC 14, HP 54, ATK +4, DPR 18.9, SV +5  
**Minion**: AC/DC 11, HP 16, ATK +4, DPR 14, SV +2  

### CR 3
**Normal**: AC/DC 13, HP 60, ATK +4, DPR 21, SV +4  
**Artillery**: AC/DC 11, HP 48, ATK +4, DPR 12.6, SV +2, AOE DPR 9  
**Brute**: AC/DC 11, HP 72, ATK +6, DPR 25.2, SV +4  
**Elite**: AC/DC 14, HP 81, ATK +4, DPR 28.35, SV +5  
**Minion**: AC/DC 11, HP 24, ATK +4, DPR 21, SV +2  

### CR 4
**Normal**: AC/DC 14, HP 80, ATK +5, DPR 28, SV +5  
**Artillery**: AC/DC 12, HP 64, ATK +5, DPR 16.8, SV +3, AOE DPR 12  
**Brute**: AC/DC 12, HP 96, ATK +7, DPR 33.6, SV +5  
**Elite**: AC/DC 15, HP 108, ATK +5, DPR 37.8, SV +6  
**Minion**: AC/DC 12, HP 32, ATK +5, DPR 28, SV +3  

### CR 5
**Normal**: AC/DC 14, HP 100, ATK +5, DPR 35, SV +5  
**Artillery**: AC/DC 12, HP 80, ATK +5, DPR 21, SV +3, AOE DPR 15  
**Brute**: AC/DC 12, HP 120, ATK +7, DPR 42, SV +5  
**Elite**: AC/DC 15, HP 135, ATK +5, DPR 47.25, SV +6  
**Minion**: AC/DC 12, HP 40, ATK +5, DPR 35, SV +3  

### CR 6

**Normal**: AC/DC 15, HP 120, ATK +6, DPR 42, SV +6  
**Caster**: HP 84, AC/DC 14, ATK +5, DPR 46.2, SV +7  
**Skirmisher**: HP 96, AC/DC 16, ATK +8, DPR 37.8, SV +6  
**Defender**: HP 156, AC/DC 17, ATK +5, DPR 33.6, SV +8  
**Assassin**: HP 72, AC/DC 15, ATK +9, DPR 54.6, SV +5  
**Artillery**: AC/DC 13, HP 96, ATK +6, DPR 25.2, SV +4, AOE DPR 18  
**Brute**: AC/DC 13, HP 144, ATK +8, DPR 50.4, SV +6  
**Elite**: AC/DC 16, HP 162, ATK +6, DPR 56.7, SV +7, AOE DPR 18  
**Minion**: AC/DC 13, HP 48, ATK +6, DPR 42, SV +4  
**Boss**: HP 180, AC/DC 17, ATK +7, DPR 52.5, SV +8, AOE DPR 18  
**Uberboss**: HP 240, AC/DC 18, ATK +8, DPR 63, SV +9, AOE DPR 18  
**Boss Pair**: HP 150, AC/DC 16, ATK +6.5, DPR 46.2, SV +7

### CR 7

**Normal**: AC/DC 15, HP 140, ATK +6, DPR 49, SV +6  
**Caster**: HP 98, AC/DC 14, ATK +5, DPR 53.9, SV +7  
**Skirmisher**: HP 112, AC/DC 16, ATK +8, DPR 44.1, SV +6  
**Defender**: HP 182, AC/DC 17, ATK +5, DPR 39.2, SV +8  
**Assassin**: HP 84, AC/DC 15, ATK +9, DPR 63.7, SV +5  
**Artillery**: AC/DC 13, HP 112, ATK +6, DPR 29.4, SV +4, AOE DPR 21  
**Brute**: AC/DC 13, HP 168, ATK +8, DPR 58.8, SV +6  
**Elite**: AC/DC 16, HP 189, ATK +6, DPR 66.15, SV +7, AOE DPR 21  
**Minion**: AC/DC 13, HP 56, ATK +6, DPR 49, SV +4  
**Boss**: HP 210, AC/DC 17, ATK +7, DPR 61.25, SV +8, AOE DPR 21  
**Uberboss**: HP 280, AC/DC 18, ATK +8, DPR 73.5, SV +9, AOE DPR 21  
**Boss Pair**: HP 175, AC/DC 16, ATK +6.5, DPR 53.9, SV +7

### CR 8

**Normal**: AC/DC 16, HP 160, ATK +7, DPR 56, SV +7  
**Caster**: HP 112, AC/DC 15, ATK +6, DPR 61.6, SV +8  
**Skirmisher**: HP 128, AC/DC 17, ATK +9, DPR 50.4, SV +7  
**Defender**: HP 208, AC/DC 18, ATK +6, DPR 44.8, SV +9  
**Assassin**: HP 96, AC/DC 16, ATK +10, DPR 72.8, SV +6  
**Artillery**: AC/DC 14, HP 128, ATK +7, DPR 33.6, SV +5, AOE DPR 24  
**Brute**: AC/DC 14, HP 192, ATK +9, DPR 67.2, SV +7  
**Elite**: AC/DC 17, HP 216, ATK +7, DPR 75.6, SV +8, AOE DPR 24  
**Minion**: AC/DC 14, HP 64, ATK +7, DPR 56, SV +5  
**Boss**: HP 240, AC/DC 18, ATK +8, DPR 70, SV +9, AOE DPR 24  
**Uberboss**: HP 320, AC/DC 19, ATK +9, DPR 84, SV +10, AOE DPR 24  
**Boss Pair**: HP 200, AC/DC 17, ATK +7.5, DPR 61.6, SV +8

### CR 9

**Normal**: AC/DC 16, HP 180, ATK +7, DPR 63, SV +7  
**Caster**: HP 126, AC/DC 15, ATK +6, DPR 69.3, SV +8  
**Skirmisher**: HP 144, AC/DC 17, ATK +9, DPR 56.7, SV +7  
**Defender**: HP 234, AC/DC 18, ATK +6, DPR 50.4, SV +9  
**Assassin**: HP 108, AC/DC 16, ATK +10, DPR 81.9, SV +6  
**Artillery**: AC/DC 14, HP 144, ATK +7, DPR 37.8, SV +5, AOE DPR 27  
**Brute**: AC/DC 14, HP 216, ATK +9, DPR 75.6, SV +7  
**Elite**: AC/DC 17, HP 243, ATK +7, DPR 85.05, SV +8, AOE DPR 27  
**Minion**: AC/DC 14, HP 72, ATK +7, DPR 63, SV +5  
**Boss**: HP 270, AC/DC 18, ATK +8, DPR 78.75, SV +9, AOE DPR 27  
**Uberboss**: HP 360, AC/DC 19, ATK +9, DPR 94.5, SV +10, AOE DPR 27  
**Boss Pair**: HP 225, AC/DC 17, ATK +7.5, DPR 69.3, SV +8

### CR 10

**Normal**: AC/DC 17, HP 200, ATK +8, DPR 70, SV +8  
**Caster**: HP 140, AC/DC 16, ATK +7, DPR 77, SV +9  
**Skirmisher**: HP 160, AC/DC 18, ATK +10, DPR 63, SV +8  
**Defender**: HP 260, AC/DC 19, ATK +7, DPR 56, SV +10  
**Assassin**: HP 120, AC/DC 17, ATK +11, DPR 91, SV +7  
**Artillery**: AC/DC 15, HP 160, ATK +8, DPR 42, SV +6, AOE DPR 30  
**Brute**: AC/DC 15, HP 240, ATK +10, DPR 84, SV +8  
**Elite**: AC/DC 18, HP 270, ATK +8, DPR 94.5, SV +9, AOE DPR 30  
**Minion**: AC/DC 15, HP 80, ATK +8, DPR 70, SV +6  
**Boss**: HP 300, AC/DC 19, ATK +9, DPR 87.5, SV +10, AOE DPR 30  
**Uberboss**: HP 400, AC/DC 20, ATK +10, DPR 105, SV +11, AOE DPR 30  
**Boss Pair**: HP 250, AC/DC 18, ATK +8.5, DPR 77, SV +9

### CR 11

**Normal**: AC/DC 17, HP 220, ATK +8, DPR 77, SV +8  
**Caster**: HP 154, AC/DC 16, ATK +7, DPR 84.7, SV +9  
**Skirmisher**: HP 176, AC/DC 18, ATK +10, DPR 69.3, SV +8  
**Defender**: HP 286, AC/DC 19, ATK +7, DPR 61.6, SV +10  
**Assassin**: HP 132, AC/DC 17, ATK +11, DPR 100.1, SV +7  
**Artillery**: AC/DC 15, HP 176, ATK +8, DPR 46.2, SV +6, AOE DPR 33  
**Brute**: AC/DC 15, HP 264, ATK +10, DPR 92.4, SV +8  
**Elite**: AC/DC 18, HP 297, ATK +8, DPR 104.85, SV +9, AOE DPR 33  
**Minion**: AC/DC 15, HP 88, ATK +8, DPR 77, SV +6  
**Boss**: HP 330, AC/DC 19, ATK +9, DPR 96.25, SV +10, AOE DPR 33  
**Uberboss**: HP 440, AC/DC 20, ATK +10, DPR 115.5, SV +11, AOE DPR 33  
**Boss Pair**: HP 275, AC/DC 18, ATK +8.5, DPR 84.7, SV +9

### CR 12

**Normal**: AC/DC 18, HP 240, ATK +9, DPR 84, SV +9  
**Caster**: HP 168, AC/DC 17, ATK +8, DPR 92.4, SV +10  
**Skirmisher**: HP 192, AC/DC 19, ATK +11, DPR 75.6, SV +9  
**Defender**: HP 312, AC/DC 20, ATK +8, DPR 67.2, SV +11  
**Assassin**: HP 144, AC/DC 18, ATK +12, DPR 109.2, SV +8  
**Artillery**: AC/DC 16, HP 192, ATK +9, DPR 50.4, SV +7, AOE DPR 36  
**Brute**: AC/DC 16, HP 288, ATK +11, DPR 100.8, SV +9  
**Elite**: AC/DC 19, HP 324, ATK +9, DPR 114.3, SV +10, AOE DPR 36  
**Minion**: AC/DC 16, HP 96, ATK +9, DPR 84, SV +7  
**Boss**: HP 360, AC/DC 20, ATK +10, DPR 105, SV +11, AOE DPR 36  
**Uberboss**: HP 480, AC/DC 21, ATK +11, DPR 126, SV +12, AOE DPR 36  
**Boss Pair**: HP 300, AC/DC 19, ATK +9.5, DPR 92.4, SV +10

### CR 13

**Normal**: AC/DC 18, HP 260, ATK +9, DPR 91, SV +9  
**Caster**: HP 182, AC/DC 17, ATK +8, DPR 100.1, SV +10  
**Skirmisher**: HP 208, AC/DC 19, ATK +11, DPR 81.9, SV +9  
**Defender**: HP 338, AC/DC 20, ATK +8, DPR 72.8, SV +11  
**Assassin**: HP 156, AC/DC 18, ATK +12, DPR 118.3, SV +8  
**Artillery**: AC/DC 16, HP 208, ATK +9, DPR 54.6, SV +7, AOE DPR 39  
**Brute**: AC/DC 16, HP 312, ATK +11, DPR 109.2, SV +9  
**Elite**: AC/DC 19, HP 351, ATK +9, DPR 123.75, SV +10, AOE DPR 39  
**Minion**: AC/DC 16, HP 104, ATK +9, DPR 91, SV +7  
**Boss**: HP 390, AC/DC 20, ATK +10, DPR 113.75, SV +11, AOE DPR 39  
**Uberboss**: HP 520, AC/DC 21, ATK +11, DPR 136.5, SV +12, AOE DPR 39  
**Boss Pair**: HP 325, AC/DC 19, ATK +9.5, DPR 100.1, SV +10

### CR 14

**Normal**: AC/DC 19, HP 280, ATK +10, DPR 98, SV +10  
**Caster**: HP 196, AC/DC 18, ATK +9, DPR 107.8, SV +11  
**Skirmisher**: HP 224, AC/DC 20, ATK +12, DPR 88.2, SV +10  
**Defender**: HP 364, AC/DC 21, ATK +9, DPR 78.4, SV +12  
**Assassin**: HP 168, AC/DC 19, ATK +13, DPR 127.4, SV +9  
**Artillery**: AC/DC 17, HP 224, ATK +10, DPR 58.8, SV +8, AOE DPR 42  
**Brute**: AC/DC 17, HP 336, ATK +12, DPR 117.6, SV +10  
**Elite**: AC/DC 20, HP 378, ATK +10, DPR 132.3, SV +11, AOE DPR 42  
**Minion**: AC/DC 17, HP 112, ATK +10, DPR 98, SV +8  
**Boss**: HP 420, AC/DC 21, ATK +11, DPR 122.5, SV +12, AOE DPR 42  
**Uberboss**: HP 560, AC/DC 22, ATK +12, DPR 147, SV +13, AOE DPR 42  
**Boss Pair**: HP 350, AC/DC 20, ATK +10.5, DPR 107.8, SV +11

### CR 15

**Normal**: AC/DC 19, HP 300, ATK +10, DPR 105, SV +10  
**Caster**: HP 210, AC/DC 18, ATK +9, DPR 115.5, SV +11  
**Skirmisher**: HP 240, AC/DC 20, ATK +12, DPR 94.5, SV +10  
**Defender**: HP 390, AC/DC 21, ATK +9, DPR 84, SV +12  
**Assassin**: HP 180, AC/DC 19, ATK +13, DPR 136.5, SV +9  
**Artillery**: AC/DC 17, HP 240, ATK +10, DPR 63, SV +8, AOE DPR 45  
**Brute**: AC/DC 17, HP 360, ATK +12, DPR 126, SV +10  
**Elite**: AC/DC 20, HP 405, ATK +10, DPR 141.75, SV +11, AOE DPR 45  
**Minion**: AC/DC 17, HP 120, ATK +10, DPR 105, SV +8  
**Boss**: HP 450, AC/DC 21, ATK +11, DPR 131.25, SV +12, AOE DPR 45  
**Uberboss**: HP 600, AC/DC 22, ATK +12, DPR 157.5, SV +13, AOE DPR 45  
**Boss Pair**: HP 375, AC/DC 20, ATK +10.5, DPR 115.5, SV +11

### CR 17
**Normal**: AC/DC 20, HP 340, ATK +11, DPR 119, SV +11  
**Caster**: HP 238, AC/DC 19, ATK +10, DPR 131.6, SV +12  
**Skirmisher**: HP 272, AC/DC 21, ATK +13, DPR 107.1, SV +11  
**Defender**: HP 442, AC/DC 22, ATK +10, DPR 95.2, SV +13  
**Assassin**: HP 204, AC/DC 20, ATK +14, DPR 154.7, SV +10  
**Artillery**: AC/DC 18, HP 272, ATK +11, DPR 71.4, SV +9, AOE DPR 51  
**Brute**: AC/DC 18, HP 408, ATK +13, DPR 142.8, SV +11  
**Elite**: AC/DC 21, HP 459, ATK +11, DPR 160.65, SV +12, AOE DPR 51  
**Minion**: AC/DC 18, HP 136, ATK +11, DPR 119, SV +9  
**Boss**: HP 510, AC/DC 22, ATK +12, DPR 148.75, SV +13, AOE DPR 51  
**Uberboss**: HP 680, AC/DC 23, ATK +13, DPR 178.5, SV +14, AOE DPR 51  
**Boss Pair**: HP 425, AC/DC 21, ATK +11.5, DPR 131.6, SV +12  
**Omegaboss**: HP 1190, AC/DC 24, ATK +15, DPR 297.5, AOE DPR 76.5, SV +16

### CR 19
**Normal**: AC/DC 21, HP 380, ATK +12, DPR 133, SV +12  
**Caster**: HP 266, AC/DC 20, ATK +11, DPR 147.8, SV +13  
**Skirmisher**: HP 304, AC/DC 22, ATK +14, DPR 119.7, SV +12  
**Defender**: HP 494, AC/DC 23, ATK +11, DPR 106.4, SV +14  
**Assassin**: HP 228, AC/DC 21, ATK +15, DPR 172.9, SV +11  
**Artillery**: AC/DC 19, HP 304, ATK +12, DPR 79.8, SV +10, AOE DPR 57  
**Brute**: AC/DC 19, HP 456, ATK +14, DPR 159.6, SV +12  
**Elite**: AC/DC 22, HP 513, ATK +12, DPR 179.55, SV +13, AOE DPR 57  
**Minion**: AC/DC 19, HP 152, ATK +12, DPR 133, SV +10  
**Boss**: HP 570, AC/DC 23, ATK +13, DPR 166.25, SV +14, AOE DPR 57  
**Uberboss**: HP 760, AC/DC 24, ATK +14, DPR 199.5, SV +15, AOE DPR 57  
**Boss Pair**: HP 475, AC/DC 22, ATK +12.5, DPR 147.8, SV +13  
**Omegaboss**: HP 1330, AC/DC 25, ATK +16, DPR 332.5, AOE DPR 85.5, SV +17


### CR 21
**Normal**: AC/DC 22, HP 420, ATK +13, DPR 147, SV +13  
**Caster**: HP 294, AC/DC 21, ATK +12, DPR 164.9, SV +14  
**Skirmisher**: HP 336, AC/DC 23, ATK +15, DPR 132.3, SV +13  
**Defender**: HP 546, AC/DC 24, ATK +12, DPR 117.6, SV +15  
**Assassin**: HP 252, AC/DC 22, ATK +16, DPR 191.1, SV +12  
**Artillery**: AC/DC 20, HP 336, ATK +13, DPR 88.2, SV +11, AOE DPR 63  
**Brute**: AC/DC 20, HP 504, ATK +15, DPR 176.4, SV +13  
**Elite**: AC/DC 23, HP 567, ATK +13, DPR 198.45, SV +14, AOE DPR 63  
**Minion**: AC/DC 20, HP 168, ATK +13, DPR 147, SV +11  
**Boss**: HP 630, AC/DC 24, ATK +14, DPR 183.75, SV +15, AOE DPR 63  
**Uberboss**: HP 840, AC/DC 25, ATK +15, DPR 220.5, SV +16, AOE DPR 63  
**Boss Pair**: HP 525, AC/DC 23, ATK +13.5, DPR 164.9, SV +14  
**Omegaboss**: HP 1470, AC/DC 26, ATK +17, DPR 367.5, AOE DPR 94.5, SV +18

### CR 23
**Normal**: AC/DC 23, HP 460, ATK +14, DPR 161, SV +14  
**Caster**: HP 322, AC/DC 22, ATK +13, DPR 182, SV +15  
**Skirmisher**: HP 368, AC/DC 24, ATK +16, DPR 144.9, SV +14  
**Defender**: HP 598, AC/DC 25, ATK +13, DPR 128.8, SV +16  
**Assassin**: HP 276, AC/DC 23, ATK +17, DPR 209.3, SV +13  
**Artillery**: AC/DC 21, HP 368, ATK +14, DPR 96.6, SV +12, AOE DPR 69  
**Brute**: AC/DC 21, HP 552, ATK +16, DPR 193.2, SV +14  
**Elite**: AC/DC 24, HP 621, ATK +14, DPR 217.35, SV +15, AOE DPR 69  
**Minion**: AC/DC 21, HP 184, ATK +14, DPR 161, SV +12  
**Boss**: HP 690, AC/DC 25, ATK +15, DPR 201.25, SV +16, AOE DPR 69  
**Uberboss**: HP 920, AC/DC 26, ATK +16, DPR 241.5, SV +17, AOE DPR 69  
**Boss Pair**: HP 575, AC/DC 24, ATK +14.5, DPR 182, SV +15
**Omegaboss**: HP 1610, AC/DC 27, ATK +18, DPR 402.5, AOE DPR 103.5, SV +19










### Short Form Formulas for Statblocks

1. **Normal Statblock**:
   - **AC**: 12 + (1/2 × CR)
   - **HP**: 20 × CR
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR (or 2d6 per CR)
   - **DPR (Multiple-Target)**: 3 × CR (or 1d6 per CR)
   - **Saving Throw (Proficiency)**: 3 + (1/2 × CR)
   - **Saving Throw DC**: 12 + (1/2 × CR)

2. **Artillery**:
   - **AC**: (12 + 1/2 × CR) - 2
   - **HP**: 20 × CR × 0.8
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 0.6
   - **DPR (Multiple-Target)**: 3 × CR (or 1d6 per CR)
   - **Saving Throw**: (3 + 1/2 × CR) - 2
   - **AOE DPR**: 3 × CR (or 1d6 per CR)

3. **Brute**:
   - **AC**: (12 + 1/2 × CR) - 2
   - **HP**: 20 × CR × 1.2
   - **Attack Bonus**: (3 + 1/2 × CR) + 2
   - **DPR (Single-Target)**: 7 × CR × 1.2
   - **DPR (Multiple-Target)**: 3 × CR × 1.2
   - **Saving Throw**: 3 + (1/2 × CR)

4. **Support**:
   - **AC**: (12 + 1/2 × CR) + 2
   - **HP**: 20 × CR × 0.8
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 0.6
   - **DPR (Multiple-Target)**: 3 × CR × 0.6
   - **Saving Throw**: (3 + 1/2 × CR) + 2

5. **Elite**:
   - **AC**: (12 + 1/2 × CR) + 1
   - **HP**: 20 × CR × 1.35
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 1.35
   - **DPR (Multiple-Target)**: 3 × CR × 1.35
   - **Saving Throw**: (3 + 1/2 × CR) + 1
   - **AOE DPR**: 3 × CR (or 1d6 per CR)

6. **Minion**:
   - **AC**: (12 + 1/2 × CR) - 2
   - **HP**: 20 × CR × 0.4
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR
   - **DPR (Multiple-Target)**: 3 × CR
   - **Saving Throw**: (3 + 1/2 × CR) - 2

7. **Boss**:
   - **AC**: (12 + 1/2 × CR)
   - **HP**: 20 × CR × 1.5
   - **Attack Bonus**: 3 + (1/2 × CR)
   - **DPR (Single-Target)**: 7 × CR × 1.5
   - **DPR (Multiple-Target)**: 3 × CR × 1.5
   - **Saving Throw**: (3 + 1/2 × CR) + 2
   - **AOE DPR**: 3 × CR (or 1d6 per CR)
   - **Lair Action**: Initiative Count 20
   - **Legendary Actions**:3
   - **Legendary Resistance**:3/day
   - **Boss Action**:Initiative Count 10

8. **Uber Boss**:
   - **AC**: (12 + 1/2 × CR) + 2
   - **HP**: 20 × CR × 2
   - **Attack Bonus**: (3 + 1/2 × CR) + 2
   - **DPR (Single-Target)**: 7 × CR × 2
   - **DPR (Multiple-Target)**: 3 × CR × 2
   - **Saving Throw**: (3 + 1/2 × CR) + 3
   - **AOE DPR**: 3 × CR (or 1d6 per CR)
   - **Lair Action**: Initiative Count 20
   - **Legendary Actions**:3(CR6-12), 5(CR13-17)
   - **Legendary Resistance**:3/day(CR6-12),5/day(CR+13)
   - **Uber Action**:Initiative Count 10 and 1

9. **Omega Boss**:
   - **AC**: (12 + 1/2 × CR) + 4
   - **HP**: 20 × CR × 3.5
   - **Attack Bonus**: (3 + 1/2 × CR) + 4
   - **DPR (Single-Target)**: 7 × CR × 2.5
   - **DPR (Multiple-Target)**: 3 × CR × 2.5
   - **Saving Throw**: (3 + 1/2 × CR) + 5
   - **AOE DPR**: 3 × CR × 1.5 (or 1d6 per CR × 1.5)
   - **Lair Action**: Initiative Count 20
   - **Legendary Actions**:5(CR+17)
   - **Legendary Resistance**:1/round(CR17-20), 2/round(CR+21)
   - **Omega Action**:Initiative Count 10, 5, and 1