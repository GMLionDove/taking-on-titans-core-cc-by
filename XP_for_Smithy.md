This is an example of the town upgrade mechanics. 

You must use XP to upgrade a part of town, (example smithy), and that part of town needs to be upgraded to make use of recipes. (Upgrade smithy from tier 0 to tier 1 to allow silvered items.)

Recipes and raw material must be secured from bounties. (silver mine cleared in bounty for silvered weapons)

Town parts (the smithy) cannot be the same or higher tier than the town itself.

Town tier is upgraded by spending XP after bounties.

Town XP threshold = the cumulative cost of every character level of the tier, times the westmarch tier multiplier.

westmarch tier multiplier is based on the number of active players and GMs in the westmarch. multiplier starts at half the number of players and GMs (26 concurrent players and GMs is x13)


Town requires 13(300+900+2700) =50700 xp, 0r 51000 rounded up, 50000 rounded down, experience points to level up from tier 0 to tier 1.

town parts require just the last level of the tier times half the number of players (2700x13= 35000xp for 26 players to upgrade the smithy to tier 1)

bounties give extra xp to offset the cost, or alternatively, fund the town as a sixth/seventh party member.

