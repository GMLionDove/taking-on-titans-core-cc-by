### Rules and Mechanics for Augments
**Augment System Overview**:
The Augment System allows characters to enhance their abilities, spells, and class features through powerful modifications known as augments. These augments can be acquired through various means, including defeating powerful foes, completing significant quests, and discovering ancient relics.
### Types of Augments
1. **Ability Augments**:
   - Enhance or modify class abilities and features.
   - Examples include changing damage types, adding new effects, or combining features.

2. **Spell Augments**:
   - Modify spells to increase their effectiveness or change their behavior.
   - Examples include altering damage types, expanding areas of effect, or adding new spell effects.

3. **Feature Fusion**:
   - Combine two or more class features into a single, enhanced feature.
   - Allows for unique and powerful combinations that redefine a character's capabilities.

### Augment Tiers

Augments are divided into tiers based on their power and the level of the challenges overcome to obtain them. Higher-tier augments provide more significant benefits and require greater achievements to unlock.

1. **Tier 1 Augments**:
   - Unlocked by defeating CR 6-8 Uberbosses.
   - Provide basic enhancements and modifications.

2. **Tier 2 Augments**:
   - Unlocked by defeating CR 9-12 Uberbosses.
   - Offer more substantial improvements and new abilities.

3. **Tier 3 Augments**:
   - Unlocked by defeating CR 13-16 Uberbosses.
   - Grant powerful enhancements and transformative effects.

4. **Tier 4 Augments**:
   - Unlocked by defeating CR 17-20 Uberbosses.
   - Provide major enhancements and unique abilities.

5. **Tier 5 Augments**:
   - Unlocked by defeating CR 21+ Uberbosses.
   - Offer the most powerful and game-changing modifications.

6. **Omega Augments**:
   - Unlocked by defeating CR 17+ Omegabosses.
   - Represent the pinnacle of power and provide unparalleled enhancements.

### Acquisition and Costs

**XP Coins**:
- Augments are purchased using XP Coins, which are earned by defeating powerful foes and completing significant quests.
- The cost of augments scales with their tier, reflecting their power and rarity.

**Limitations**:
- Characters can only have a limited number of augments active at any time, typically one or two for each tier.
- Augments cannot be stacked unless explicitly stated otherwise.

### Mechanics

1. **Applying Augments**:
   - Augments can be applied during a long rest.
   - Characters can switch out augments they have previously acquired, but doing so requires another long rest.

2. **Combining Augments**:
   - Some augments can be combined to create unique effects. This requires a special process, usually involving a significant quest or challenge.

3. **Augment Interaction**:
   - Augments can interact with each other to create synergistic effects.
   - Careful planning is required to maximize the benefits of multiple augments.






When considering the difference between class features and subclass features for augments, we can use a general rule of thumb based on their overall impact and availability:

### General Weighting Guidelines

1. **Class Features**: 
   - These are core abilities available to all members of a class, typically balanced to be universally applicable and powerful.
   - **Weighting**: Class features should generally be considered more impactful and thus cost more to augment.

2. **Subclass Features**: 
   - These are specialized abilities that define a particular subclass, providing unique flavor and utility within specific contexts.
   - **Weighting**: Subclass features are more niche and less universally applicable, so they should cost less to augment compared to class features.

### Weighting Factors:
1. **Level of Acquisition**:
   - Features gained at higher levels are generally more powerful or impactful.
   - **Example**: A feature gained at level 3 might be less impactful than one gained at level 10.

2. **Frequency of Use**:
   - Features that can be used frequently (e.g., at-will or multiple times per short rest) are generally more impactful.
   - **Example**: A feature that provides a constant passive bonus might be more valuable than a feature usable once per long rest.

3. **Utility and Versatility**:
   - Features that provide broad utility or can be used in a variety of situations are generally more impactful.
   - **Example**: A feature that boosts multiple skills or saves vs. a feature that provides a single skill bonus.

### Example Augments

#### Ability Augments:
1. **Class Features**:
   - **Extra Attack Augment**: Gain an additional attack as a bonus action.
     - **Cost**: High (as Extra Attack is a significant feature for combat classes)
   - **Rage Enhancement**: When raging, also cast Enlarge on yourself.
     - **Cost**: High (as Rage is a core feature of Barbarians)

2. **Subclass Features**:
   - **Divine Smite Augment**: Allow Divine Smite to deal an additional damage type (e.g., fire or cold).
     - **Cost**: Moderate (as Divine Smite is a significant Paladin feature but more niche)
   - **Wild Shape Augment**: Increase the CR of creatures you can transform into by 1.
     - **Cost**: Moderate (as Wild Shape is significant for Druids but specialized)

#### Feature Augments:
1. **Class Features**:
   - **Ki Recovery**: Spend ki points to expend a hit die and recover HP equal to the roll + Constitution modifier.
     - **Cost**: High (as it affects the Monk’s core resource management)
   - **Superiority Dice Replacement**: Use hit dice in place of superiority dice and gain extra hit dice equal to your Constitution modifier.
     - **Cost**: High (as it affects the Fighter’s Battle Master maneuvers significantly)

2. **Subclass Features**:
   - **Eldritch Invocation Augment**: Gain an additional Eldritch Invocation.
     - **Cost**: Moderate (as it provides more options but is limited to Warlocks)
   - **Channel Divinity Augment**: Use Channel Divinity an additional time per short rest.
     - **Cost**: Moderate (as it’s a significant boost for Clerics but limited by short rest)

### Example Pricing:

1. **Minor Augments**: 500 XP Coins
   - Minor changes that provide small but meaningful enhancements.
   - **Examples**: Changing the damage type of a spell, small increases to skill bonuses.

2. **Moderate Augments**: 1,000 XP Coins
   - More impactful changes that significantly enhance an ability or feature.
   - **Examples**: Allowing Divine Smite to deal an additional damage type, gaining an additional Eldritch Invocation.

3. **Major Augments**: 2,500 XP Coins
   - Major enhancements that provide significant boosts or additional uses.
   - **Examples**: Extra Attack as a bonus action, using hit dice in place of superiority dice.

By considering the level of acquisition, frequency of use, and utility/versatility of class and subclass features, we can better balance the cost of augments to ensure they are meaningful and appropriately challenging to obtain.

# Augment Multiclassing

Restricting multiclassing to a greater augment system could be an interesting way to add depth and customization to characters while maintaining balance and thematic consistency. This approach allows players to gain some benefits of multiclassing without the complexities and potential balance issues that can arise from full multiclassing. Here are some thoughts on how to implement this system effectively:

### Benefits of the Greater Augment System for Multiclassing

1. **Controlled Power Levels**:
   - **Balance**: By restricting multiclassing through augments, you can better control the power level of characters and prevent some of the more broken combinations that can occur with traditional multiclassing.
   - **Customization**: Players still have the opportunity to customize their characters and gain abilities from other classes, but in a more balanced and controlled manner.

2. **Thematic Consistency**:
   - **Story Integration**: Augments can be tied to the campaign's story and setting, making them more thematic and integrated into the world.
   - **Character Development**: This approach allows for a more cohesive character progression that aligns with the campaign's narrative.

3. **Simplified Mechanics**:
   - **Ease of Use**: Managing augments is often simpler than dealing with the complexities of multiclassing, making it easier for both players and DMs.
   - **Focus on Core Classes**: Players can focus on developing their primary class while still gaining some abilities from other classes.

### Implementing the Greater Augment System for Multiclassing

1. **Designing the Augments**:
   - **Balanced Abilities**: Ensure that the abilities granted by augments are balanced and don't overshadow the core class features.
   - **Thematic Choices**: Create augments that align with the themes and abilities of other classes, allowing players to gain a taste of those classes' features without full multiclassing.

2. **Tiered Augments**:
   - **Minor Augments**: Offer small, thematic abilities that provide a taste of another class (e.g., a fighter gaining a minor spellcasting ability similar to a wizard's cantrips).
   - **Moderate Augments**: Provide more substantial abilities that reflect a deeper connection to another class (e.g., a rogue gaining a limited version of a monk's ki abilities).
   - **Greater Augments**: Allow access to more powerful abilities that require significant investment (e.g., a cleric gaining the ability to rage like a barbarian for a limited number of times per day).

3. **Cost and Progression**:
   - **Experience or Resource Cost**: Determine the cost for acquiring these augments, such as spending XP Coins or other resources.
   - **Progression Limits**: Set limits on how many and how powerful augments a character can have based on their level to ensure balance.

4. **Integration with the Campaign**:
   - **Narrative Justification**: Ensure that the acquisition of augments is justified within the campaign's story, such as through quests, special training, or magical artifacts.
   - **DM Approval**: Require DM approval for augments to ensure they fit within the campaign's balance and narrative.

### Example Augments

#### Minor Augments
- **Magic Initiate (Wizard)**: Gain two cantrips from the wizard spell list.
- **Martial Training (Fighter)**: Gain proficiency with one type of martial weapon and light armor.

#### Moderate Augments
- **Divine Blessing (Cleric)**: Gain the ability to cast a first-level cleric spell once per day.
- **Ki Adept (Monk)**: Gain access to a limited pool of ki points, which can be used for basic monk abilities like Flurry of Blows or Step of the Wind.

#### Greater Augments
- **Barbarian's Rage**: Gain the ability to rage like a barbarian, but only a limited number of times per day.
- **Rogue's Cunning Action**: Gain the rogue's Cunning Action ability, allowing you to dash, disengage, or hide as a bonus action.

### Conclusion

Restricting multiclassing to a greater augment system can be a creative and effective way to offer customization and versatility without the complexities and balance issues of traditional multiclassing. By carefully designing and balancing the augments, you can provide players with the ability to enhance their characters in thematic and meaningful ways that align with your campaign's narrative and power level. This approach encourages creative character builds and provides a structured way to integrate diverse abilities into the game.