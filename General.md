Taking on Titans General

## HP and Limbs
1. Standard HP is only for vital areas( Head, torso).
2. Limbs have a break DC that lowers when threshold damage is done.
    Ex. Glow Giant 180HP, 4 Limbs, Break DC 25 threshold 25.

## Climbing Gear: To get up there!
Huge- Grappling Hook
Gargantuan- Harpoon Crossbow
Titanic- Zipline Ballista

## Ankle Breaker: To bring the vital parts to you!
Heavy Property- +1d4 to overcome threshold and Break DC.
Great Weapon Property- +1d6 to overcome threshold and Break DC.
Siege Weapon Property- Ignore threshold, +10 to overcome Break DC.

## Heavy Armaments- To survive it!
Greatshields- Reduce foe's STR bonus to ATK.
Juggernaut Armor- Reduce foe's STR bonus to DMG.

## Size Effects: Bigger is badder!
    Size Modifier(SM) (Huge +2, Gargantuan +6)
Quake DC=10+(1/2CR)+(SM)
Sweep DC=8+(1/2CR)+(SM)
Roar  DC=8+CR+(SM)

## Natural Armor: Thick hides save lives!
Natural Armor = AC-(10+DEXmod+armor)
Natural Armor = Damage Reduction (DR)
DR = Bonus to Physical Saves (STR, DEX, CON)
DR reduces damage from All sources unless Natuaral Armor HP is reduced to 0 in that Vital Area.
Natural Armor HP = CR x DR
Natural Armor HP must be Depleted for every called shot (eyes, throat, heart)
    Example: Glow Giant CR 5, AC 17, DR 6, Natural Armor HP(NHP) 30

## Armored Giants: Tankier than a tank!
Vital parts under armor have full cover.
Armor must be broken or removed to damage vital parts.
Armor HP depends on the size and enhancement bonus.
Huge Armor HP = 20x(AC Bonus 1 to 9)+25
Gargantuan Armor HP = 35x(AC Bonus 1 to 9)+80

Magical Armor can only be damaged by magical weapons.
Magical Armor Bonus Hitpoints
+1 Armor = +80 HP
+2 Armor = +160 HP
+3 Armor = +320 HP



